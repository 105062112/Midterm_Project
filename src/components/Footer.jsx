import './Footer.css';

export default class Footer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            this.props.show && (<ButtonGroup className='footer fixed-bottom container' size='lg'>
                <Button color='light' onClick={this.props.onFriendClick} className='w-25 ml-0' active={this.props.page === 'Friend'}>
                    <i className='fas fa-user'></i>
                </Button>
                <Button color='light' onClick={this.props.onAddClick} className='w-25 ml-0' active={this.props.page === 'Add'}>
                    <i className='fas fa-user-plus'></i>
                </Button>
                <Button color='light' onClick={this.props.onChatClick} className='w-25 ml-0' active={this.props.page === 'Chat'}>
                    <i className='fas fa-comment'></i>
                </Button>
                <Button color='light' onClick={this.props.onSettingsClick} className='w-25 ml-0' active={this.props.page === 'Settings'}>
                    <i className='fas fa-cog'></i>
                </Button>
            </ButtonGroup>)
        );
    }
}