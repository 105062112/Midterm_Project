import './Room.css';

export default class Room extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            input: '',
            dialog: [],
            cancelSubscribe: null
        };
    }

    render() {
        return (
            <div className='room position-relative' ref='ref'>
                <div className='roomHeader fixed-top text-dark bg-light p-2 d-flex justify-content-between align-items-center'>
                    <div className='hideRoom' onClick={this.props.onHideRoom}>
                        <i className='fas fa-chevron-left'></i>
                    </div>
                    {this.props.room.displayName}
                    <div className='invisible'>
                        <i className='fas fa-cog'></i>
                    </div>
                </div>
                <div className='dialog px-3'>
                    {this.state.dialog.map(this.dialogToDOM)}
                </div>
                <div className='input fixed-bottom container'>
                    <InputGroup>
                        <Input type='text' value={this.state.input} onChange={this.handleInputChange} onFocus={this.handleInputFocus} />
                        <InputGroupAddon addonType='append'>
                            <Button color='primary' onClick={this.handleSend}>
                                <i className='fas fa-location-arrow'></i>
                            </Button>
                        </InputGroupAddon>
                    </InputGroup>
                </div>
            </div>
        );
    }

    componentDidMount() {
        this.setState({
            cancelSubscribe: 
                this.props.room.ref.collection('dialog').orderBy('timestamp').onSnapshot(dialog => {
                    this.setState({
                        dialog: dialog.docs.slice().map(d => ({id: d.id, ...d.data()}))
                    });
                })
        });
    }
    
    componentWillUnmount() {
        this.state.cancelSubscribe();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.refs.ref.scrollTop = this.refs.ref.scrollHeight;
    }

    handleInputChange = e => {
        this.setState({
            input: e.target.value
        });
    };

    handleSend = () => {
        this.setState({
            input: ''
        });
        this.props.onSend(this.state.input, this.props.room);
    };

    dialogToDOM = dialog => {
        let fromMe = dialog.from === this.props.user.uid;
        return (
            <div key={dialog.id} className={`my-3 ml-2 d-flex ${fromMe ? 'justify-content-end' : 'justify-content-start'} align-items-start`}>
                {
                    !fromMe && <div className='sticker' style={{ backgroundImage: `url(${this.props.room.photoURL})` }}></div>
                }
                <div className='text-dark bg-light px-3 py-2 mx-2 rounded' style={{maxWidth: '70%'}}>
                    {dialog.content}
                </div>
            </div>
        );
    };
}