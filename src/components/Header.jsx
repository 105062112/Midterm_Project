import './Header.css';

export default class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className='header fixed-top bg-light'>
                <h1 className='d-flex justify-content-center my-2'>
                    <a href='/'>
                        <span className='text-primary'>Chat</span>
                        <span className='text-info'>TW</span>
                    </a>
                </h1>
            </div>
        );
    }
}