import './Friend.css';

export default class Friend extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={'friend' + (this.props.show ? ' show' : '')}>
                {this.props.friendsList.sort((a, b) => a.type - b.type).map(this.friendToDOM)}
            </div>
        );
    }

    friendToDOM = friend => {
        const typeToBtn = [
            (<div className='ml-auto'>
                <Button color='success' size='sm' onClick={this.handleAgreeFriend.bind(this, friend)}>
                    <i className='fas fa-check' style={{ width: '14px' }}></i>
                </Button>
                <Button color='danger' size='sm' className='ml-1' onClick={this.handleRejectFriend.bind(this, friend)}>
                    <i className='fas fa-times' style={{ width: '14px' }}></i>
                </Button>
            </div>),
            <Button color='primary' size='sm' className='ml-auto' disabled>
                <i className='fas fa-user-plus' style={{ width: '14px' }}></i>
            </Button>,
            <Button color='info' size='sm' className='ml-auto' onClick={this.handleChatWithFriend.bind(this, friend)}>
                <i className='fas fa-comments' style={{ width: '14px' }}></i>
            </Button>
        ];
        return (
            <div key={friend.id} className='d-flex align-items-center mb-2' >
                <div className='sticker' style={{ backgroundImage: `url(${friend.photoURL})` }}></div>
                <div className='ml-3 ellipsis' style={{width: 'calc(100% - 10rem)'}}>
                    {friend.displayName}<br />
                    <small className='text-secondary ellipsis'>{friend.email}</small>
                </div>
                {typeToBtn[friend.type]}
            </div>
        );
    };

    handleAgreeFriend = friend => this.props.onAgreeFriend(friend);
    handleRejectFriend = friend => this.props.onRejectFriend(friend);
    handleChatWithFriend = friend => this.props.onChatWithFriend(friend);
}