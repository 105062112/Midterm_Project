import './Search.css';

export default class Search extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className='search position-relative'>
                <i className='fas fa-search text-dark position-absolute'></i>
                <div className='d-flex'>
                    <Input type='search' className='d-inline-block' value={this.props.input} onChange={this.props.onInputChange} placeholder={this.props.placeholder} />
                    <Button color='primary' className='ml-2' onClick={this.props.onSearchClick}>搜尋</Button>
                </div>
            </div>
        );
    }
}