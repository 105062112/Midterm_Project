/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"index": 0
/******/ 	};
/******/
/******/ 	// script path function
/******/ 	function jsonpScriptSrc(chunkId) {
/******/ 		return __webpack_require__.p + "" + ({}[chunkId]||chunkId) + ".bundle.js"
/******/ 	}
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([0,"vendors~index"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "../node_modules/css-loader/index.js??ref--5-1!./components/Add.css":
/*!*****************************************************************!*\
  !*** ../node_modules/css-loader??ref--5-1!./components/Add.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".add .sticker {\n  width: 10rem;\n  height: 10rem;\n}", ""]);

// exports


/***/ }),

/***/ "../node_modules/css-loader/index.js??ref--5-1!./components/Chat.css":
/*!******************************************************************!*\
  !*** ../node_modules/css-loader??ref--5-1!./components/Chat.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".chat .sticker {\n  width: 4rem;\n  min-width: 4rem;\n  height: 4rem;\n}\n\n.chat>div:not(.room) {\n  background: #ffffff07;\n}\n\n.chat>div:not(.room):hover {\n  background: #ffffff09;\n}\n\n.room-item {\n  border-radius: 1rem;\n}", ""]);

// exports


/***/ }),

/***/ "../node_modules/css-loader/index.js??ref--5-1!./components/Footer.css":
/*!********************************************************************!*\
  !*** ../node_modules/css-loader??ref--5-1!./components/Footer.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".footer button {\n  border-bottom-left-radius: 0 !important;\n  border-bottom-right-radius: 0 !important;\n}\n\n.footer button:focus {\n  box-shadow: none !important;\n}", ""]);

// exports


/***/ }),

/***/ "../node_modules/css-loader/index.js??ref--5-1!./components/Friend.css":
/*!********************************************************************!*\
  !*** ../node_modules/css-loader??ref--5-1!./components/Friend.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".friend .sticker {\n  width: 4rem;\n  min-width: 4rem;\n  height: 4rem;\n}", ""]);

// exports


/***/ }),

/***/ "../node_modules/css-loader/index.js??ref--5-1!./components/Header.css":
/*!********************************************************************!*\
  !*** ../node_modules/css-loader??ref--5-1!./components/Header.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".header {\n  width: 100vw;\n}\n\n.header a,\n.header a:hover {\n  text-decoration: none !important;\n}", ""]);

// exports


/***/ }),

/***/ "../node_modules/css-loader/index.js??ref--5-1!./components/Login.css":
/*!*******************************************************************!*\
  !*** ../node_modules/css-loader??ref--5-1!./components/Login.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".login {\n  max-width: 21rem;\n}", ""]);

// exports


/***/ }),

/***/ "../node_modules/css-loader/index.js??ref--5-1!./components/Main.css":
/*!******************************************************************!*\
  !*** ../node_modules/css-loader??ref--5-1!./components/Main.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".main {\n  padding-top: 5rem;\n  padding-bottom: 4rem;\n  min-height: 100vh;\n}", ""]);

// exports


/***/ }),

/***/ "../node_modules/css-loader/index.js??ref--5-1!./components/Pages.css":
/*!*******************************************************************!*\
  !*** ../node_modules/css-loader??ref--5-1!./components/Pages.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".pages>div:not(.footer):not(.show) {\n  position: absolute;\n  left: -10000rem;\n}", ""]);

// exports


/***/ }),

/***/ "../node_modules/css-loader/index.js??ref--5-1!./components/Room.css":
/*!******************************************************************!*\
  !*** ../node_modules/css-loader??ref--5-1!./components/Room.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".room,\n.room:hover {\n  height: 100vh;\n  overflow: scroll;\n  background: #ffffff07;\n  border-radius: 1rem;\n  margin-top: 4rem;\n}\n\n.room .roomHeader {\n  height: 4rem;\n}\n\n.room .roomHeader>div {\n  width: 1.5rem;\n  height: 2rem;\n  line-height: 2rem;\n  text-align: center;\n}\n\n.room textarea {\n  resize: none;\n}\n\n.room .dialog {\n  margin-bottom: 8.5rem;\n}\n\n.room .rounded {\n  border-radius: 1rem !important;\n}\n\n.chat .room .sticker {\n  width: 3em;\n  min-width: 3rem;\n  height: 3rem;\n}", ""]);

// exports


/***/ }),

/***/ "../node_modules/css-loader/index.js??ref--5-1!./components/Search.css":
/*!********************************************************************!*\
  !*** ../node_modules/css-loader??ref--5-1!./components/Search.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".search svg {\n  top: 0.7rem;\n  left: 0.8rem;\n}\n\n.search input {\n  padding-left: 2.5rem;\n}", ""]);

// exports


/***/ }),

/***/ "../node_modules/css-loader/index.js??ref--5-1!./components/Settings.css":
/*!**********************************************************************!*\
  !*** ../node_modules/css-loader??ref--5-1!./components/Settings.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".settings .sticker {\n  width: 10rem;\n  height: 10rem;\n}\n\n.settings label {\n  padding-right: 0;\n}\n\n.setings input {\n  padding-left: 0;\n}", ""]);

// exports


/***/ }),

/***/ "../node_modules/css-loader/index.js??ref--5-1!./index.css":
/*!********************************************************!*\
  !*** ../node_modules/css-loader??ref--5-1!./index.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../node_modules/css-loader/lib/css-base.js */ "../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "* {\n  box-sizing: border-box;\n}\n\nbody {\n  font-family: -apple-system, system-ui, BlinkMacSystemFont, \"Segoe UI\", Roboto, \"Helvetica Neue\", Arial, sans-serif;\n}\n\n@media (min-width: 768px) {\n  .container {\n    max-width: 720px !important;\n  }\n}\n\n.sticker {\n  border-radius: 100%;\n  background-size: cover;\n  background-position: 50% 50%;\n  background-repeat: no-repeat;\n}\n\n.ellipsis {\n  text-overflow: ellipsis;\n  overflow: hidden;\n  display: inline-block;\n  white-space: nowrap;\n  width: 100%;\n}", ""]);

// exports


/***/ }),

/***/ "./components/Add.css":
/*!****************************!*\
  !*** ./components/Add.css ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader??ref--5-1!./Add.css */ "../node_modules/css-loader/index.js??ref--5-1!./components/Add.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./components/Add.jsx":
/*!****************************!*\
  !*** ./components/Add.jsx ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(React, Button) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Search = __webpack_require__(/*! components/Search.jsx */ "./components/Search.jsx");

var _Search2 = _interopRequireDefault(_Search);

__webpack_require__(/*! ./Add.css */ "./components/Add.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Add = function (_React$Component) {
    _inherits(Add, _React$Component);

    function Add(props) {
        _classCallCheck(this, Add);

        var _this = _possibleConstructorReturn(this, (Add.__proto__ || Object.getPrototypeOf(Add)).call(this, props));

        _this.resultToDOM = function (result) {
            var typeToBtn = {
                strangers: React.createElement(
                    Button,
                    { color: 'primary', className: 'mt-2', onClick: _this.handleAddFriend.bind(_this, result) },
                    React.createElement('i', { className: 'fas fa-user-plus' })
                ),
                friends: React.createElement(
                    Button,
                    { color: 'info', className: 'mt-2', onClick: _this.handleChatWithFriend.bind(_this, result) },
                    React.createElement('i', { className: 'fas fa-comments' })
                ),
                request: React.createElement(
                    'div',
                    null,
                    React.createElement(
                        Button,
                        { color: 'success', className: 'mx-1 mt-2', onClick: _this.handleAgreeFriend.bind(_this, result) },
                        React.createElement('i', { className: 'fas fa-check' })
                    ),
                    React.createElement(
                        Button,
                        { color: 'danger', className: 'mx-1 mt-2', onClick: _this.handleRejectFriend.bind(_this, result) },
                        React.createElement('i', { className: 'fas fa-times', style: { width: '14px' } })
                    )
                ),
                sent: React.createElement(
                    Button,
                    { color: 'primary', className: 'mt-2', disabled: true },
                    React.createElement('i', { className: 'fas fa-user-plus' })
                )
            };
            return React.createElement(
                'div',
                { key: result.id, className: 'd-flex flex-column justify-content-center align-items-center mt-4 py-4 rounded', style: { background: '#ffffff07' } },
                React.createElement('div', { className: 'sticker my-3', style: { backgroundImage: 'url(' + result.photoURL + ')' } }),
                result.displayName,
                React.createElement(
                    'small',
                    { className: 'text-secondary' },
                    result.type === 'me' ? '能找到自己嗎？' : result.email
                ),
                typeToBtn[result.type]
            );
        };

        _this.handleAddFriend = function (result) {
            return _this.props.onAddFriend(result);
        };

        _this.handleAgreeFriend = function (result) {
            return _this.props.onAgreeFriend(result);
        };

        _this.handleRejectFriend = function (result) {
            return _this.props.onRejectFriend(result);
        };

        _this.handleChatWithFriend = function (result) {
            return _this.props.onChatWithFriend(result);
        };

        return _this;
    }

    _createClass(Add, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'add' + (this.props.show ? ' show' : '') },
                React.createElement(_Search2.default, { input: this.props.input, onInputChange: this.props.onInputChange,
                    onSearchClick: this.props.onSearchClick, placeholder: '\u8F38\u5165\u4FE1\u7BB1\u3001\u540D\u7A31\u6216\u624B\u6A5F' }),
                React.createElement(
                    'div',
                    { className: 'result' },
                    this.props.results1.map(this.resultToDOM),
                    this.props.results2.map(this.resultToDOM),
                    this.props.results3.map(this.resultToDOM)
                )
            );
        }
    }]);

    return Add;
}(React.Component);

exports.default = Add;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! react */ "../node_modules/react/index.js"), __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["Button"]))

/***/ }),

/***/ "./components/Chat.css":
/*!*****************************!*\
  !*** ./components/Chat.css ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader??ref--5-1!./Chat.css */ "../node_modules/css-loader/index.js??ref--5-1!./components/Chat.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./components/Chat.jsx":
/*!*****************************!*\
  !*** ./components/Chat.jsx ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(React) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Room = __webpack_require__(/*! components/Room.jsx */ "./components/Room.jsx");

var _Room2 = _interopRequireDefault(_Room);

__webpack_require__(/*! ./Chat.css */ "./components/Chat.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Chat = function (_React$Component) {
    _inherits(Chat, _React$Component);

    function Chat(props) {
        _classCallCheck(this, Chat);

        var _this = _possibleConstructorReturn(this, (Chat.__proto__ || Object.getPrototypeOf(Chat)).call(this, props));

        _this.roomToDOM = function (room) {
            return React.createElement(
                'div',
                { key: room.id, className: 'room-item d-flex align-items-center mb-3 px-3 py-3', onClick: _this.props.onShowRoomClick.bind(_this, room) },
                React.createElement('div', { className: 'sticker', style: { backgroundImage: 'url(' + room.photoURL + ')' } }),
                React.createElement(
                    'div',
                    { className: 'ml-3 ellipsis' },
                    room.displayName,
                    React.createElement('br', null),
                    React.createElement(
                        'small',
                        { className: 'text-secondary ellipsis' },
                        room.lastDialog
                    )
                )
            );
        };

        return _this;
    }

    _createClass(Chat, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'chat ' + (this.props.show ? ' show' : '') + ' ' + (this.props.showRoom ? ' container fixed-top mt-3' : '') },
                this.props.showRoom && React.createElement(_Room2.default, { room: this.props.showWhichRoom, onHideRoom: this.props.onHideRoom, onSend: this.props.onSend, user: this.props.user }),
                !this.props.showRoom && this.props.chatroomsList.map(this.roomToDOM)
            );
        }
    }]);

    return Chat;
}(React.Component);

exports.default = Chat;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! react */ "../node_modules/react/index.js")))

/***/ }),

/***/ "./components/Footer.css":
/*!*******************************!*\
  !*** ./components/Footer.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader??ref--5-1!./Footer.css */ "../node_modules/css-loader/index.js??ref--5-1!./components/Footer.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./components/Footer.jsx":
/*!*******************************!*\
  !*** ./components/Footer.jsx ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(React, ButtonGroup, Button) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

__webpack_require__(/*! ./Footer.css */ "./components/Footer.css");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Footer = function (_React$Component) {
    _inherits(Footer, _React$Component);

    function Footer(props) {
        _classCallCheck(this, Footer);

        return _possibleConstructorReturn(this, (Footer.__proto__ || Object.getPrototypeOf(Footer)).call(this, props));
    }

    _createClass(Footer, [{
        key: 'render',
        value: function render() {
            return this.props.show && React.createElement(
                ButtonGroup,
                { className: 'footer fixed-bottom container', size: 'lg' },
                React.createElement(
                    Button,
                    { color: 'light', onClick: this.props.onFriendClick, className: 'w-25 ml-0', active: this.props.page === 'Friend' },
                    React.createElement('i', { className: 'fas fa-user' })
                ),
                React.createElement(
                    Button,
                    { color: 'light', onClick: this.props.onAddClick, className: 'w-25 ml-0', active: this.props.page === 'Add' },
                    React.createElement('i', { className: 'fas fa-user-plus' })
                ),
                React.createElement(
                    Button,
                    { color: 'light', onClick: this.props.onChatClick, className: 'w-25 ml-0', active: this.props.page === 'Chat' },
                    React.createElement('i', { className: 'fas fa-comment' })
                ),
                React.createElement(
                    Button,
                    { color: 'light', onClick: this.props.onSettingsClick, className: 'w-25 ml-0', active: this.props.page === 'Settings' },
                    React.createElement('i', { className: 'fas fa-cog' })
                )
            );
        }
    }]);

    return Footer;
}(React.Component);

exports.default = Footer;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! react */ "../node_modules/react/index.js"), __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["ButtonGroup"], __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["Button"]))

/***/ }),

/***/ "./components/Friend.css":
/*!*******************************!*\
  !*** ./components/Friend.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader??ref--5-1!./Friend.css */ "../node_modules/css-loader/index.js??ref--5-1!./components/Friend.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./components/Friend.jsx":
/*!*******************************!*\
  !*** ./components/Friend.jsx ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(React, Button) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

__webpack_require__(/*! ./Friend.css */ "./components/Friend.css");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Friend = function (_React$Component) {
    _inherits(Friend, _React$Component);

    function Friend(props) {
        _classCallCheck(this, Friend);

        var _this = _possibleConstructorReturn(this, (Friend.__proto__ || Object.getPrototypeOf(Friend)).call(this, props));

        _this.friendToDOM = function (friend) {
            var typeToBtn = [React.createElement(
                'div',
                { className: 'ml-auto' },
                React.createElement(
                    Button,
                    { color: 'success', size: 'sm', onClick: _this.handleAgreeFriend.bind(_this, friend) },
                    React.createElement('i', { className: 'fas fa-check', style: { width: '14px' } })
                ),
                React.createElement(
                    Button,
                    { color: 'danger', size: 'sm', className: 'ml-1', onClick: _this.handleRejectFriend.bind(_this, friend) },
                    React.createElement('i', { className: 'fas fa-times', style: { width: '14px' } })
                )
            ), React.createElement(
                Button,
                { color: 'primary', size: 'sm', className: 'ml-auto', disabled: true },
                React.createElement('i', { className: 'fas fa-user-plus', style: { width: '14px' } })
            ), React.createElement(
                Button,
                { color: 'info', size: 'sm', className: 'ml-auto', onClick: _this.handleChatWithFriend.bind(_this, friend) },
                React.createElement('i', { className: 'fas fa-comments', style: { width: '14px' } })
            )];
            return React.createElement(
                'div',
                { key: friend.id, className: 'd-flex align-items-center mb-2' },
                React.createElement('div', { className: 'sticker', style: { backgroundImage: 'url(' + friend.photoURL + ')' } }),
                React.createElement(
                    'div',
                    { className: 'ml-3 ellipsis', style: { width: 'calc(100% - 10rem)' } },
                    friend.displayName,
                    React.createElement('br', null),
                    React.createElement(
                        'small',
                        { className: 'text-secondary ellipsis' },
                        friend.email
                    )
                ),
                typeToBtn[friend.type]
            );
        };

        _this.handleAgreeFriend = function (friend) {
            return _this.props.onAgreeFriend(friend);
        };

        _this.handleRejectFriend = function (friend) {
            return _this.props.onRejectFriend(friend);
        };

        _this.handleChatWithFriend = function (friend) {
            return _this.props.onChatWithFriend(friend);
        };

        return _this;
    }

    _createClass(Friend, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'friend' + (this.props.show ? ' show' : '') },
                this.props.friendsList.sort(function (a, b) {
                    return a.type - b.type;
                }).map(this.friendToDOM)
            );
        }
    }]);

    return Friend;
}(React.Component);

exports.default = Friend;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! react */ "../node_modules/react/index.js"), __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["Button"]))

/***/ }),

/***/ "./components/Header.css":
/*!*******************************!*\
  !*** ./components/Header.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader??ref--5-1!./Header.css */ "../node_modules/css-loader/index.js??ref--5-1!./components/Header.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./components/Header.jsx":
/*!*******************************!*\
  !*** ./components/Header.jsx ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(React) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

__webpack_require__(/*! ./Header.css */ "./components/Header.css");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Header = function (_React$Component) {
    _inherits(Header, _React$Component);

    function Header(props) {
        _classCallCheck(this, Header);

        return _possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).call(this, props));
    }

    _createClass(Header, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'header fixed-top bg-light' },
                React.createElement(
                    'h1',
                    { className: 'd-flex justify-content-center my-2' },
                    React.createElement(
                        'a',
                        { href: '/' },
                        React.createElement(
                            'span',
                            { className: 'text-primary' },
                            'Chat'
                        ),
                        React.createElement(
                            'span',
                            { className: 'text-info' },
                            'TW'
                        )
                    )
                )
            );
        }
    }]);

    return Header;
}(React.Component);

exports.default = Header;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! react */ "../node_modules/react/index.js")))

/***/ }),

/***/ "./components/Login.css":
/*!******************************!*\
  !*** ./components/Login.css ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader??ref--5-1!./Login.css */ "../node_modules/css-loader/index.js??ref--5-1!./components/Login.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./components/Login.jsx":
/*!******************************!*\
  !*** ./components/Login.jsx ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(React, firebase, Alert, FormGroup, Input, Button) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

__webpack_require__(/*! ./Login.css */ "./components/Login.css");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Login = function (_React$Component) {
    _inherits(Login, _React$Component);

    function Login(props) {
        _classCallCheck(this, Login);

        var _this = _possibleConstructorReturn(this, (Login.__proto__ || Object.getPrototypeOf(Login)).call(this, props));

        _this.handleEmailChange = function (e) {
            _this.setState({
                email: e.target.value
            });
        };

        _this.handlePasswordChange = function (e) {
            _this.setState({
                password: e.target.value
            });
        };

        _this.handleSignInClick = function () {
            firebase.auth().signInWithEmailAndPassword(_this.state.email, _this.state.password).then(function (user) {
                if (!user.emailVerified) {
                    firebase.auth().signOut();
                    throw {
                        code: 'auth/email-not-verified',
                        message: '您的電子信箱尚未驗證！'
                    };
                }
            }).catch(function (error) {
                var errorMessage = '';
                switch (error.code) {
                    case 'auth/invalid-email':
                        errorMessage = '信箱格式有誤！';
                        break;
                    case 'auth/user-disabled':
                        errorMessage = '此用戶已被停權！';
                        break;
                    case 'auth/user-not-found':
                        errorMessage = '查無此用戶！請先註冊！';
                        break;
                    case 'auth/wrong-password':
                        errorMessage = '密碼不正確！';
                        break;
                    default:
                        errorMessage = error.message;
                }
                _this.setState({
                    password: '',
                    isSuccess: false,
                    errorMessage: errorMessage,
                    isError: true
                });
            });
        };

        _this.handleSignInGoogleClick = function () {
            var provider = new firebase.auth.GoogleAuthProvider();
            firebase.auth().signInWithPopup(provider).catch(function (error) {
                var errorMessage = '';
                switch (error.code) {
                    case 'auth/account-exists-with-different-credential':
                        errorMessage = '此帳戶已經存在！';
                        break;
                    case 'auth/auth-domain-config-required':
                        errorMessage = '資料庫初始化失敗！';
                        break;
                    case 'auth/cancelled-popup-request':
                        errorMessage = '登入遭到取消！';
                        break;
                    case 'auth/operation-not-allowed':
                        errorMessage = '不支援的登入方式！';
                        break;
                    case 'auth/operation-not-supported-in-this-environment':
                        errorMessage = '不支援此環境！';
                        break;
                    case 'auth/popup-blocked':
                        errorMessage = '登入視窗遭瀏覽器阻擋！';
                        break;
                    case 'auth/popup-closed-by-user':
                        errorMessage = '登入視窗遭到關閉！';
                        break;
                    case 'auth/unauthorized-domain':
                        errorMessage = '此網域未經認證！';
                        break;
                    default:
                        errorMessage = error.message;
                }
                _this.setState({
                    password: '',
                    isSuccess: false,
                    errorMessage: errorMessage,
                    isError: true
                });
            });
        };

        _this.handleSignUpClick = function () {
            firebase.auth().createUserWithEmailAndPassword(_this.state.email, _this.state.password).then(function (user) {
                _this.setState({
                    password: '',
                    isError: false,
                    successMessage: '註冊成功！請登入您的信箱進行驗證',
                    isSuccess: true
                }, function () {
                    user.sendEmailVerification().then(function () {
                        firebase.auth().signOut();
                    }).catch(function (error) {
                        _this.setState({
                            password: '',
                            isSuccess: false,
                            errorMessage: error.message,
                            isError: true
                        });
                    });
                });
            }).catch(function (error) {
                var errorMessage = '';
                switch (error.code) {
                    case 'auth/invalid-email':
                        errorMessage = '信箱格式有誤！';
                        break;
                    case 'auth/email-already-in-use':
                        errorMessage = '此信箱已被使用！';
                        break;
                    case 'auth/weak-password':
                        errorMessage = '密碼長度不足6字元！';
                        break;
                    default:
                        errorMessage = error.message;
                }
                _this.setState({
                    password: '',
                    isSuccess: false,
                    errorMessage: errorMessage,
                    isError: true
                });
            });
        };

        _this.state = {
            email: '',
            password: '',
            isError: false,
            errorMessage: '',
            isSuccess: false,
            successMessage: ''
        };
        return _this;
    }

    _createClass(Login, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'login mx-auto mt-3' },
                React.createElement(
                    Alert,
                    { color: 'danger', isOpen: this.state.isError },
                    this.state.errorMessage
                ),
                React.createElement(
                    Alert,
                    { color: 'success', isOpen: this.state.isSuccess },
                    this.state.successMessage
                ),
                React.createElement(
                    FormGroup,
                    null,
                    React.createElement(Input, { type: 'email', value: this.state.email, onChange: this.handleEmailChange, placeholder: '\u96FB\u5B50\u4FE1\u7BB1', bsSize: 'lg' })
                ),
                React.createElement(
                    FormGroup,
                    null,
                    React.createElement(Input, { type: 'password', value: this.state.password, onChange: this.handlePasswordChange, placeholder: '\u5BC6\u78BC', bsSize: 'lg' })
                ),
                React.createElement(
                    FormGroup,
                    null,
                    React.createElement(
                        Button,
                        { color: 'primary', size: 'lg', block: true, onClick: this.handleSignInClick },
                        '\u767B\u5165'
                    ),
                    React.createElement(
                        Button,
                        { color: 'danger', size: 'lg', block: true, onClick: this.handleSignInGoogleClick },
                        'Google \u767B\u5165'
                    ),
                    React.createElement(
                        Button,
                        { color: 'secondary', size: 'lg', block: true, onClick: this.handleSignUpClick },
                        '\u8A3B\u518A'
                    )
                )
            );
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            this.props.onSignInSuccess();
        }
    }]);

    return Login;
}(React.Component);

exports.default = Login;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! react */ "../node_modules/react/index.js"), __webpack_require__(/*! firebase/app */ "../node_modules/firebase/app/index.js"), __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["Alert"], __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["FormGroup"], __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["Input"], __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["Button"]))

/***/ }),

/***/ "./components/Main.css":
/*!*****************************!*\
  !*** ./components/Main.css ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader??ref--5-1!./Main.css */ "../node_modules/css-loader/index.js??ref--5-1!./components/Main.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./components/Main.jsx":
/*!*****************************!*\
  !*** ./components/Main.jsx ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(React, firebase, profile) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Header = __webpack_require__(/*! components/Header.jsx */ "./components/Header.jsx");

var _Header2 = _interopRequireDefault(_Header);

var _Login = __webpack_require__(/*! components/Login.jsx */ "./components/Login.jsx");

var _Login2 = _interopRequireDefault(_Login);

var _Pages = __webpack_require__(/*! components/Pages.jsx */ "./components/Pages.jsx");

var _Pages2 = _interopRequireDefault(_Pages);

__webpack_require__(/*! ./Main.css */ "./components/Main.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Main = function (_React$Component) {
    _inherits(Main, _React$Component);

    function Main(props) {
        _classCallCheck(this, Main);

        var _this = _possibleConstructorReturn(this, (Main.__proto__ || Object.getPrototypeOf(Main)).call(this, props));

        _this.handleSignInSuccess = function () {
            firebase.firestore().doc('users/' + _this.state.user.uid).get().then(function (doc) {
                if (!doc.exists) {
                    doc.ref.set({
                        email: _this.state.user.email,
                        displayName: _this.state.user.email.replace(/@.+/, ''),
                        photoURL: _this.state.user.photoURL ? _this.state.user.photoURL : profile,
                        phoneNumber: _this.state.user.phoneNumber ? _this.state.user.phoneNumber : ''
                    });
                }
            });
        };

        _this.handleShowRoom = function () {
            _this.setState({
                showRoom: true
            });
        };

        _this.handleHideRoom = function () {
            _this.setState({
                showRoom: false
            });
        };

        _this.state = {
            user: null,
            ready: false,
            showRoom: false
        };
        return _this;
    }

    _createClass(Main, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'main container text-light' },
                !this.state.showRoom && React.createElement(_Header2.default, { show: !this.state.showRoom }),
                this.state.ready && (this.state.user ? React.createElement(_Pages2.default, { user: this.state.user, showRoom: this.state.showRoom,
                    onShowRoom: this.handleShowRoom, onHideRoom: this.handleHideRoom }) : React.createElement(_Login2.default, { onSignInSuccess: this.handleSignInSuccess }))
            );
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            firebase.auth().onAuthStateChanged(function (user) {
                _this2.setState({
                    user: user && user.emailVerified ? user : null,
                    ready: true
                });
            });
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            firebase.auth().onAuthStateChanged(function () {});
        }
    }]);

    return Main;
}(React.Component);

exports.default = Main;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! react */ "../node_modules/react/index.js"), __webpack_require__(/*! firebase/app */ "../node_modules/firebase/app/index.js"), __webpack_require__(/*! images/profile.jpg */ "./images/profile.jpg")))

/***/ }),

/***/ "./components/Pages.css":
/*!******************************!*\
  !*** ./components/Pages.css ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader??ref--5-1!./Pages.css */ "../node_modules/css-loader/index.js??ref--5-1!./components/Pages.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./components/Pages.jsx":
/*!******************************!*\
  !*** ./components/Pages.jsx ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(React, firebase) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Friend = __webpack_require__(/*! components/Friend.jsx */ "./components/Friend.jsx");

var _Friend2 = _interopRequireDefault(_Friend);

var _Add = __webpack_require__(/*! components/Add.jsx */ "./components/Add.jsx");

var _Add2 = _interopRequireDefault(_Add);

var _Chat = __webpack_require__(/*! components/Chat.jsx */ "./components/Chat.jsx");

var _Chat2 = _interopRequireDefault(_Chat);

var _Settings = __webpack_require__(/*! components/Settings.jsx */ "./components/Settings.jsx");

var _Settings2 = _interopRequireDefault(_Settings);

var _Footer = __webpack_require__(/*! components/Footer.jsx */ "./components/Footer.jsx");

var _Footer2 = _interopRequireDefault(_Footer);

__webpack_require__(/*! ./Pages.css */ "./components/Pages.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Pages = function (_React$Component) {
    _inherits(Pages, _React$Component);

    function Pages(props) {
        _classCallCheck(this, Pages);

        var _this = _possibleConstructorReturn(this, (Pages.__proto__ || Object.getPrototypeOf(Pages)).call(this, props));

        _this.handleFriendClick = function () {
            return _this.setState({ page: 'Friend' });
        };

        _this.handleAddClick = function () {
            return _this.setState({ page: 'Add' });
        };

        _this.handleChatClick = function () {
            return _this.setState({ page: 'Chat' });
        };

        _this.handleSettingsClick = function () {
            return _this.setState({ page: 'Settings' });
        };

        _this.updateFriendsList = function () {
            var friendsList = [];
            var promiseList = [];
            var determineType = function determineType(friend) {
                return friend.get('accepted') ? 2 : !friend.get('isRequest') ? 1 : 0;
            };

            var _loop = function _loop(friend) {
                if (determineType(friend) !== 1) {
                    promiseList.push(firebase.firestore().doc('users/' + friend.id).get().then(function (docSnapshot) {
                        friendsList.push(_extends({
                            id: docSnapshot.id,
                            type: determineType(friend)
                        }, docSnapshot.data()));
                    }));
                }
            };

            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = _this.state.friends[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var friend = _step.value;

                    _loop(friend);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            Promise.all(promiseList).then(function () {
                return _this.setState({ friendsList: friendsList.slice() });
            });
        };

        _this.handleAddInputChange = function (e) {
            return _this.setState({ addSearchInput: e.target.value });
        };

        _this.handleAddSearchClick = function () {
            if (_this.state.addSearchInput) {
                firebase.firestore().collection('users').where('displayName', '==', _this.state.addSearchInput).get().then(_this.updateAddResults).then(function (arr) {
                    return _this.setState({ addSearchResults1: arr });
                });
                firebase.firestore().collection('users').where('email', '==', _this.state.addSearchInput).get().then(_this.updateAddResults).then(function (arr) {
                    return _this.setState({ addSearchResults2: arr });
                });
                firebase.firestore().collection('users').where('phoneNumber', '==', _this.state.addSearchInput).get().then(_this.updateAddResults).then(function (arr) {
                    return _this.setState({ addSearchResults3: arr });
                });
            } else {
                _this.setState({
                    addSearchResults1: [],
                    addSearchResults2: [],
                    addSearchResults3: []
                });
            }
        };

        _this.updateAddResults = function (querySnapshot) {
            var arr = [];
            querySnapshot.forEach(function (docSnapshot) {
                var idx = _this.state.friends.map(function (snapshot) {
                    return snapshot.id;
                }).indexOf(docSnapshot.id);
                if (idx < 0) {
                    arr.push(_extends({ id: docSnapshot.id }, docSnapshot.data(), { type: docSnapshot.id === _this.props.user.uid ? 'me' : 'strangers' }));
                } else if (_this.state.friends[idx].get('accepted')) {
                    arr.push(_extends({ id: docSnapshot.id }, docSnapshot.data(), { type: 'friends' }));
                } else if (_this.state.friends[idx].get('isRequest')) {
                    arr.push(_extends({ id: docSnapshot.id }, docSnapshot.data(), { type: 'request' }));
                } else {
                    arr.push(_extends({ id: docSnapshot.id }, docSnapshot.data(), { type: 'sent' }));
                }
            });
            return arr;
        };

        _this.handleAddFriend = function (user) {
            firebase.firestore().doc('users/' + _this.props.user.uid + '/friends/' + user.id).set({
                accepted: false,
                isRequest: false,
                ref: firebase.firestore().doc('users/' + user.id)
            }).then(function () {
                firebase.firestore().doc('users/' + user.id + '/friends/' + _this.props.user.uid).set({
                    accepted: false,
                    isRequest: true,
                    ref: firebase.firestore().doc('users/' + _this.props.user.uid)
                });
            });
        };

        _this.handleAgreeFriend = function (user) {
            firebase.firestore().doc('users/' + _this.props.user.uid + '/friends/' + user.id).update({ accepted: true });
            firebase.firestore().doc('users/' + user.id + '/friends/' + _this.props.user.uid).update({ accepted: true });
        };

        _this.handleRejectFriend = function (user) {
            firebase.firestore().doc('users/' + _this.props.user.uid + '/friends/' + user.id).delete();
        };

        _this.handleChatWithFriend = function (user) {
            _this.handleChatClick();
            firebase.firestore().collection('users/' + _this.props.user.uid + '/chatrooms').where('with', '==', user.id).get().then(function (chatroom) {
                if (chatroom.empty) {
                    firebase.firestore().collection('chatrooms').where('member1', '==', user.id).where('member2', '==', _this.props.user.uid).get().then(function (chatroom) {
                        if (chatroom.empty) {
                            firebase.firestore().collection('chatrooms').add({
                                member1: _this.props.user.uid,
                                member2: user.id
                            }).then(function (chatroom) {
                                return firebase.firestore().doc('users/' + _this.props.user.uid + '/chatrooms/' + chatroom.id).set({
                                    ref: chatroom,
                                    with: user.id,
                                    timestamp: firebase.firestore.FieldValue.serverTimestamp(),
                                    lastDialog: '尚無對話'
                                });
                            });
                        } else {
                            firebase.firestore().doc('users/' + _this.props.user.uid + '/chatrooms/' + chatroom.docs[0].id).set({
                                ref: chatroom.docs[0].ref,
                                with: user.id,
                                timestamp: firebase.firestore.FieldValue.serverTimestamp(),
                                lastDialog: '尚無對話'
                            });
                        }
                    });
                } else {
                    _this.handleShowRoom(_this.state.chatroomsList.find(function (el) {
                        return el.with === user.id;
                    }));
                }
            });
        };

        _this.updateChatroomsList = function () {
            var chatroomsList = [];
            var promiseList = [];

            var _loop2 = function _loop2(chatroom) {
                promiseList.push(firebase.firestore().doc('users/' + chatroom.get('with')).get().then(function (user) {
                    chatroomsList.push(_extends({
                        id: chatroom.id
                    }, chatroom.data(), user.data()));
                }));
            };

            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
                for (var _iterator2 = _this.state.chatrooms[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var chatroom = _step2.value;

                    _loop2(chatroom);
                }
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                        _iterator2.return();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }

            Promise.all(promiseList).then(function () {
                return _this.setState({ chatroomsList: chatroomsList.slice().sort(function (a, b) {
                        return b.timestamp - a.timestamp;
                    }) });
            });
        };

        _this.handleShowRoom = function (room) {
            _this.setState({
                showWhichRoom: room
            });
            _this.props.onShowRoom();
        };

        _this.handleSend = function (content, room) {
            room.ref.collection('dialog').add({
                content: content,
                from: _this.props.user.uid,
                timestamp: firebase.firestore.FieldValue.serverTimestamp()
            });
            firebase.firestore().doc('users/' + _this.props.user.uid + '/chatrooms/' + room.id).update({
                lastDialog: content,
                timestamp: firebase.firestore.FieldValue.serverTimestamp()
            });
            firebase.firestore().doc('users/' + room.with + '/chatrooms/' + room.id).set({
                ref: room.ref,
                with: _this.props.user.uid,
                lastDialog: content,
                timestamp: firebase.firestore.FieldValue.serverTimestamp()
            });
        };

        _this.state = {
            page: 'Friend',
            friends: [],
            friendsList: [],
            addSearchInput: '',
            addSearchResults1: [],
            addSearchResults2: [],
            addSearchResults3: [],
            chatrooms: [],
            chatroomsList: [],
            showWhichRoom: '',
            cancelSubscribeFriends: null,
            cancelSubscribeChatrooms: null
        };
        return _this;
    }

    _createClass(Pages, [{
        key: 'render',
        value: function render() {
            var _React$createElement;

            return React.createElement(
                'div',
                { className: 'pages' },
                React.createElement(_Friend2.default, (_React$createElement = { show: this.state.page === 'Friend', friendsList: this.state.friendsList,
                    onAgreeFriend: this.handleAgreeFriend, onRejectFriend: this.handleRejectFriend,
                    onChatWithFriend: this.handleChatWithFriend,
                    onAddFriend: this.handleAddFriend }, _defineProperty(_React$createElement, 'onAgreeFriend', this.handleAgreeFriend), _defineProperty(_React$createElement, 'onRejectFriend', this.handleRejectFriend), _defineProperty(_React$createElement, 'onChatWithFriend', this.handleChatWithFriend), _React$createElement)),
                React.createElement(_Add2.default, { show: this.state.page === 'Add', friends: this.state.friends,
                    onSearchClick: this.handleAddSearchClick, onInputChange: this.handleAddInputChange,
                    input: this.state.addSearchInput, results1: this.state.addSearchResults1,
                    results2: this.state.addSearchResults2, results3: this.state.addSearchResults3,
                    onAddFriend: this.handleAddFriend, onAgreeFriend: this.handleAgreeFriend,
                    onRejectFriend: this.handleRejectFriend, onChatWithFriend: this.handleChatWithFriend }),
                React.createElement(_Chat2.default, { show: this.state.page === 'Chat', chatroomsList: this.state.chatroomsList,
                    showRoom: this.props.showRoom, showWhichRoom: this.state.showWhichRoom,
                    onShowRoomClick: this.handleShowRoom, onHideRoom: this.props.onHideRoom,
                    onSend: this.handleSend, user: this.props.user }),
                React.createElement(_Settings2.default, { show: this.state.page === 'Settings', user: this.props.user }),
                React.createElement(_Footer2.default, { onFriendClick: this.handleFriendClick, onAddClick: this.handleAddClick,
                    onChatClick: this.handleChatClick, onSettingsClick: this.handleSettingsClick,
                    page: this.state.page, show: !this.props.showRoom })
            );
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            this.setState({
                cancelSubscribeFriends: firebase.firestore().collection('users/' + this.props.user.uid + '/friends').onSnapshot(function (snapshot) {
                    _this2.setState({
                        friends: snapshot.docs.slice()
                    }, function () {
                        _this2.handleAddSearchClick();
                        _this2.updateFriendsList();
                    });
                }),
                cancelSubscribeChatrooms: firebase.firestore().collection('users/' + this.props.user.uid + '/chatrooms').onSnapshot(function (snapshot) {
                    _this2.setState({
                        chatrooms: snapshot.docs.slice()
                    }, function () {
                        _this2.updateChatroomsList();
                    });
                })
            });
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            this.state.cancelSubscribeFriends();
            this.state.cancelSubscribeChatrooms();
        }
    }]);

    return Pages;
}(React.Component);

exports.default = Pages;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! react */ "../node_modules/react/index.js"), __webpack_require__(/*! firebase/app */ "../node_modules/firebase/app/index.js")))

/***/ }),

/***/ "./components/Room.css":
/*!*****************************!*\
  !*** ./components/Room.css ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader??ref--5-1!./Room.css */ "../node_modules/css-loader/index.js??ref--5-1!./components/Room.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./components/Room.jsx":
/*!*****************************!*\
  !*** ./components/Room.jsx ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(React, InputGroup, Input, InputGroupAddon, Button) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

__webpack_require__(/*! ./Room.css */ "./components/Room.css");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Room = function (_React$Component) {
    _inherits(Room, _React$Component);

    function Room(props) {
        _classCallCheck(this, Room);

        var _this = _possibleConstructorReturn(this, (Room.__proto__ || Object.getPrototypeOf(Room)).call(this, props));

        _this.handleInputChange = function (e) {
            _this.setState({
                input: e.target.value
            });
        };

        _this.handleSend = function () {
            _this.setState({
                input: ''
            });
            _this.props.onSend(_this.state.input, _this.props.room);
        };

        _this.dialogToDOM = function (dialog) {
            var fromMe = dialog.from === _this.props.user.uid;
            return React.createElement(
                'div',
                { key: dialog.id, className: 'my-3 ml-2 d-flex ' + (fromMe ? 'justify-content-end' : 'justify-content-start') + ' align-items-start' },
                !fromMe && React.createElement('div', { className: 'sticker', style: { backgroundImage: 'url(' + _this.props.room.photoURL + ')' } }),
                React.createElement(
                    'div',
                    { className: 'text-dark bg-light px-3 py-2 mx-2 rounded', style: { maxWidth: '70%' } },
                    dialog.content
                )
            );
        };

        _this.state = {
            input: '',
            dialog: [],
            cancelSubscribe: null
        };
        return _this;
    }

    _createClass(Room, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'room position-relative', ref: 'ref' },
                React.createElement(
                    'div',
                    { className: 'roomHeader fixed-top text-dark bg-light p-2 d-flex justify-content-between align-items-center' },
                    React.createElement(
                        'div',
                        { className: 'hideRoom', onClick: this.props.onHideRoom },
                        React.createElement('i', { className: 'fas fa-chevron-left' })
                    ),
                    this.props.room.displayName,
                    React.createElement(
                        'div',
                        { className: 'invisible' },
                        React.createElement('i', { className: 'fas fa-cog' })
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'dialog px-3' },
                    this.state.dialog.map(this.dialogToDOM)
                ),
                React.createElement(
                    'div',
                    { className: 'input fixed-bottom container' },
                    React.createElement(
                        InputGroup,
                        null,
                        React.createElement(Input, { type: 'text', value: this.state.input, onChange: this.handleInputChange, onFocus: this.handleInputFocus }),
                        React.createElement(
                            InputGroupAddon,
                            { addonType: 'append' },
                            React.createElement(
                                Button,
                                { color: 'primary', onClick: this.handleSend },
                                React.createElement('i', { className: 'fas fa-location-arrow' })
                            )
                        )
                    )
                )
            );
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            this.setState({
                cancelSubscribe: this.props.room.ref.collection('dialog').orderBy('timestamp').onSnapshot(function (dialog) {
                    _this2.setState({
                        dialog: dialog.docs.slice().map(function (d) {
                            return _extends({ id: d.id }, d.data());
                        })
                    });
                })
            });
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            this.state.cancelSubscribe();
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate(prevProps, prevState, snapshot) {
            this.refs.ref.scrollTop = this.refs.ref.scrollHeight;
        }
    }]);

    return Room;
}(React.Component);

exports.default = Room;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! react */ "../node_modules/react/index.js"), __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["InputGroup"], __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["Input"], __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["InputGroupAddon"], __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["Button"]))

/***/ }),

/***/ "./components/Search.css":
/*!*******************************!*\
  !*** ./components/Search.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader??ref--5-1!./Search.css */ "../node_modules/css-loader/index.js??ref--5-1!./components/Search.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./components/Search.jsx":
/*!*******************************!*\
  !*** ./components/Search.jsx ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(React, Input, Button) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

__webpack_require__(/*! ./Search.css */ "./components/Search.css");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Search = function (_React$Component) {
    _inherits(Search, _React$Component);

    function Search(props) {
        _classCallCheck(this, Search);

        return _possibleConstructorReturn(this, (Search.__proto__ || Object.getPrototypeOf(Search)).call(this, props));
    }

    _createClass(Search, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'search position-relative' },
                React.createElement('i', { className: 'fas fa-search text-dark position-absolute' }),
                React.createElement(
                    'div',
                    { className: 'd-flex' },
                    React.createElement(Input, { type: 'search', className: 'd-inline-block', value: this.props.input, onChange: this.props.onInputChange, placeholder: this.props.placeholder }),
                    React.createElement(
                        Button,
                        { color: 'primary', className: 'ml-2', onClick: this.props.onSearchClick },
                        '\u641C\u5C0B'
                    )
                )
            );
        }
    }]);

    return Search;
}(React.Component);

exports.default = Search;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! react */ "../node_modules/react/index.js"), __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["Input"], __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["Button"]))

/***/ }),

/***/ "./components/Settings.css":
/*!*********************************!*\
  !*** ./components/Settings.css ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader??ref--5-1!./Settings.css */ "../node_modules/css-loader/index.js??ref--5-1!./components/Settings.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./components/Settings.jsx":
/*!*********************************!*\
  !*** ./components/Settings.jsx ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(React, firebase, Label, Input, InputGroup, InputGroupAddon, Button, FormFeedback) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

__webpack_require__(/*! ./Settings.css */ "./components/Settings.css");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Settings = function (_React$Component) {
    _inherits(Settings, _React$Component);

    function Settings(props) {
        _classCallCheck(this, Settings);

        var _this = _possibleConstructorReturn(this, (Settings.__proto__ || Object.getPrototypeOf(Settings)).call(this, props));

        _this.handlePhotoUpload = function (e) {
            if (e.target.files.length) {
                firebase.storage().ref('users/' + _this.props.user.uid).put(e.target.files[0], {
                    contentType: e.target.files[0].type
                }).then(function (snapshot) {
                    firebase.firestore().doc('users/' + _this.props.user.uid).update({
                        photoURL: snapshot.downloadURL
                    }).then(function () {
                        _this.state.photoURL = snapshot.downloadURL;
                    });
                });
            }
        };

        _this.handleDisplayNameChange = function (e) {
            _this.setState({
                displayName: e.target.value
            });
        };

        _this.handlePhoneNumberChange = function (e) {
            _this.setState({
                phoneNumber: e.target.value
            });
        };

        _this.handlePasswordChange = function (e) {
            _this.setState({
                password: e.target.value
            });
        };

        _this.handleDisplayNameUpdate = function () {
            if (/^[\u4e00-\u9fa5\w]+$/.test(_this.state.displayName)) {
                firebase.firestore().doc('users/' + _this.props.user.uid).update({
                    displayName: _this.state.displayName
                }).then(function () {
                    _this.setState({
                        displayNameValid: true,
                        displayNameInvalid: false,
                        displayNameMessage: '成功修改名稱'
                    });
                });
            } else {
                _this.setState({
                    displayNameValid: false,
                    displayNameInvalid: true,
                    displayNameMessage: '名稱格式不符'
                });
            }
        };

        _this.handlePhoneNumberUpdate = function () {
            if (/^09\d{8}$/.test(_this.state.phoneNumber)) {
                firebase.firestore().doc('users/' + _this.props.user.uid).update({
                    phoneNumber: _this.state.phoneNumber
                }).then(function () {
                    _this.setState({
                        phoneNumberValid: true,
                        phoneNumberInvalid: false,
                        phoneNumberMessage: '成功修改手機'
                    });
                });
            } else {
                _this.setState({
                    phoneNumberValid: false,
                    phoneNumberInvalid: true,
                    phoneNumberMessage: '手機格式不符'
                });
            }
        };

        _this.handlePasswordUpdate = function () {
            _this.props.user.updatePassword(_this.state.password).then(function () {
                _this.setState({
                    passwordValid: true,
                    passwordInvalid: false,
                    passwordMessage: '修改密碼成功'
                });
            }).catch(function (error) {
                var errorMessage = '';
                switch (error.code) {
                    case 'auth/weak-password':
                        errorMessage = '密碼長度不足6字元';
                        break;
                    case 'auth/requires-recent-login':
                        errorMessage = '請先重新登入';
                        break;
                    default:
                        errorMessage = error.message;
                }
                _this.setState({
                    passwordValid: false,
                    passwordInvalid: true,
                    passwordMessage: errorMessage
                });
            });
        };

        _this.handleFocus = function () {
            _this.setState({
                displayNameValid: false,
                displayNameInvalid: false,
                phoneNumberValid: false,
                phoneNumberInvalid: false,
                passwordValid: false,
                passwordInvalid: false
            });
        };

        _this.handleLogoutClick = function () {
            firebase.auth().signOut();
        };

        _this.state = {
            photoURL: '',
            email: '',
            displayName: '',
            displayNameValid: false,
            displayNameInvalid: false,
            displayNameMessage: '',
            phoneNumber: '',
            phoneNumberValid: false,
            phoneNumberInvalid: false,
            phoneNumberMessage: '',
            password: '',
            passwordValid: false,
            passwordInvalid: false,
            passwordMessage: ''
        };
        return _this;
    }

    _createClass(Settings, [{
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'settings' + (this.props.show ? ' show' : '') },
                React.createElement(
                    'div',
                    { className: 'text-center mb-0' },
                    React.createElement(
                        Label,
                        { 'for': 'photoURLInput' },
                        React.createElement('div', { className: 'sticker', style: { backgroundImage: 'url(' + this.state.photoURL + ')' } })
                    ),
                    React.createElement(Input, { type: 'file', id: 'photoURLInput', className: 'd-none', accept: '.jpg, .jpeg, .png', onChange: this.handlePhotoUpload })
                ),
                React.createElement('br', null),
                React.createElement(
                    InputGroup,
                    null,
                    React.createElement(
                        InputGroupAddon,
                        { addonType: 'prepend' },
                        '\u96FB\u5B50\u4FE1\u7BB1'
                    ),
                    React.createElement(Input, { type: 'email', id: 'emailInput', value: this.state.email, disabled: true })
                ),
                React.createElement('br', null),
                React.createElement(
                    InputGroup,
                    null,
                    React.createElement(
                        InputGroupAddon,
                        { addonType: 'prepend' },
                        '\u7528\u6236\u540D\u7A31'
                    ),
                    React.createElement(Input, { type: 'text', id: 'displayNameInput', value: this.state.displayName, onChange: this.handleDisplayNameChange, onFocus: this.handleFocus, valid: this.state.displayNameValid, invalid: this.state.displayNameInvalid }),
                    React.createElement(
                        InputGroupAddon,
                        { addonType: 'append' },
                        React.createElement(
                            Button,
                            { color: 'primary', onClick: this.handleDisplayNameUpdate },
                            React.createElement('i', { className: 'fas fa-check' })
                        )
                    ),
                    React.createElement(
                        FormFeedback,
                        { className: 'text-right', valid: this.state.displayNameValid },
                        this.state.displayNameMessage
                    )
                ),
                React.createElement('br', null),
                React.createElement(
                    InputGroup,
                    null,
                    React.createElement(
                        InputGroupAddon,
                        { addonType: 'prepend' },
                        '\u624B\u6A5F\u865F\u78BC'
                    ),
                    React.createElement(Input, { type: 'text', pattern: '[0-9]*', id: 'phoneNumberInput', value: this.state.phoneNumber, placeholder: '\u5C1A\u672A\u8F38\u5165\u624B\u6A5F\u865F\u78BC', onChange: this.handlePhoneNumberChange, onFocus: this.handleFocus, valid: this.state.phoneNumberValid, invalid: this.state.phoneNumberInvalid }),
                    React.createElement(
                        InputGroupAddon,
                        { addonType: 'append' },
                        React.createElement(
                            Button,
                            { color: 'primary', onClick: this.handlePhoneNumberUpdate },
                            React.createElement('i', { className: 'fas fa-check' })
                        )
                    ),
                    React.createElement(
                        FormFeedback,
                        { className: 'text-right', valid: this.state.phoneNumberValid },
                        this.state.phoneNumberMessage
                    )
                ),
                React.createElement('br', null),
                React.createElement(
                    InputGroup,
                    null,
                    React.createElement(
                        InputGroupAddon,
                        { addonType: 'prepend' },
                        '\u66F4\u6539\u5BC6\u78BC'
                    ),
                    React.createElement(Input, { type: 'password', id: 'passwordInput', value: this.state.password, placeholder: '\u8F38\u5165\u65B0\u5BC6\u78BC', onChange: this.handlePasswordChange, onFocus: this.handleFocus, valid: this.state.passwordValid, invalid: this.state.passwordInvalid }),
                    React.createElement(
                        InputGroupAddon,
                        { addonType: 'append' },
                        React.createElement(
                            Button,
                            { color: 'primary', onClick: this.handlePasswordUpdate },
                            React.createElement('i', { className: 'fas fa-check' })
                        )
                    ),
                    React.createElement(
                        FormFeedback,
                        { className: 'text-right', valid: this.state.passwordValid },
                        this.state.passwordMessage
                    )
                ),
                React.createElement(
                    Button,
                    { color: 'danger', className: 'mt-4', block: true, onClick: this.handleLogoutClick },
                    '\u767B\u51FA'
                )
            );
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            firebase.firestore().doc('users/' + this.props.user.uid).onSnapshot(function (docSnapshot) {
                _this2.setState(_extends({}, docSnapshot.data(), {
                    password: ''
                }));
            });
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            firebase.firestore().doc('users/' + this.props.user.uid).onSnapshot(function () {});
        }
    }]);

    return Settings;
}(React.Component);

exports.default = Settings;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! react */ "../node_modules/react/index.js"), __webpack_require__(/*! firebase/app */ "../node_modules/firebase/app/index.js"), __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["Label"], __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["Input"], __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["InputGroup"], __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["InputGroupAddon"], __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["Button"], __webpack_require__(/*! reactstrap */ "../node_modules/reactstrap/dist/reactstrap.es.js")["FormFeedback"]))

/***/ }),

/***/ "./images/profile.jpg":
/*!****************************!*\
  !*** ./images/profile.jpg ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIABAMAAAAGVsnJAAAAD1BMVEVDdOCgw/88cN9mkOqHrfZMkj+aAAAIq0lEQVR42u3dW5rbKBCG4WrsBZg2CyDYC9BpAZal/a9pLjKTZzKTkyRKotDHXW7yNG//FIhGSPzJmwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPDjNs/zMAzDPM9nBJh6EZdSSilFkX44F8Dcu+jk383Ffj4NQOglyv+ak348BUBoUpIftpQuY/0AUxPl5+0y1A7QiPtF/8XFS9UA4Ze//q8EOw+DXQHu3W/7L+Iuba0Adyd/0lwc6gT4w/6LiLQ1Aizov8S2PoAl/ReJY20AoVvSf3GPsS6A0C0KwI4COwG8oixs8VoTwLS4/yJuqAcgOFkBkMZqAJoVARCRRy0Ar3X9F3etA+Auq1tbBUC3HuBRA8AU1wPsMBOoA6yaAXZcEqsDvGRTu1oHCHEbgBuNA3SysT1sA2wNgP5UKIUHQORiGeC+PQDaEdAFeGfovzztAmQJgPIGoRQfAOW1gCZAkExtNArwygXwYRSgywUgNgHylEDtmVAMjADVmVAPIOQLgOYjkR7ATTK2D4MAXU6Aiz2AnCNAcwyoAXxKVoAPcwBZR4DivoiYGAGKY0AL4CZiYwyIjRGgNwaUAIJkb6MpgHvMDtCaAnhn7797mgLIPwIkWgII+UeA1kSoA3BTSIDSRKgD0CkAKG0KiJUSoFUExEoJ0CoCYqUEaBUBFYCXCoDOH0jETA1U2hYSMyVA6cCQBsBdCcC1RgB0aqDS3rDYqYE6VVDs1ECdTREFgCBqzQhA1Oq/xlpQAeCuFgCNaUAB4FMP4IsJgLdeDXiaAOj0AB4mAPT6r/E0kB9AcRbUeBpQAIh6AM4CwF0ToDUAcFMcAgqPQ/kBPjUBvhgAeGsCPA0AdGJqHswP0JwdQFQbAMUDBF2AEYDSATQXghprYWsALQDFA+jWgPIBbroAHwCcHOBaPMDr7Al4nT0Bn7oAz+IB3mdPwJsEnByAGkANOHkCWAnyLHDyBGy5PpAEsCNUwY4Qe4JnBwjx5EOAP40BcHIAzgec/oSI6hmhhwGA058SO/05wdOfFFVdC7cGAE5/WjyYWgcZe2NE46fN/192ltZBvDXGe4P5/8ubpWUA7w7benvcBsDp7w/gBonT3yFy+ltktKYBZ+UeIa1tQTM3SWkthp9mAG52aqCl+wR1LtbVuVFSBcB5OwCdmRpo6FZZsXSrrEoRaA0BqOwLeksAnZFVgN7t8jYeBLyh7ws4W98XOP0XJvI/DlyNAWT/ykxrDaCxMQL0PrT0NjEJKgJk3hdrzQHknQf0vkCuB/CyMAfwxUnNb452FkaAja/Oqi0CdAEyPhOPNgFe5ZdAG98ed1a/PZ6rDD68VYB74atAdYA8EVANgDJAjpnQtYYBckRANwDaANsjoBwAbYDtRwUu3jbA1ghoB0AdYGsEtAOgDxDKXQTuA7Btc/Dp7QMEV+Zj4G4AG+qgegXcB2B9Hbz4OgDWDoI4VgKw8modN/haAHyzQsBdfD0AYflDkXuMFQGsKANxn/7vBbB8c2jwdQH4qcACuCvAoqkg7db/HQHC648F3NVXCOB9X8IfQo4E8FMqLP97A/jJFVT/jwDwUxd/t/5pfc0APjS/HAbpMvq6Abyffv5g4C7D7j/O/gA+TO6HBC4Ooz8DgPehl/8auCj9eMTPcgiA92HuJX2rBilJP4zH/CQHAXjvx7nvxaUUpe/n8bAf4ziAQhoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6LUwz9PU/9OGYZ7PckAizFPfuJRSSlFEnHMi8ve/L/08zzUDhLlvxCUXxUX5/pBQlBS/nReZqwSY+0bSH5wUTS6K9HNdAF/PRC15cSjFnQ4O7fLaXN/I8remXNwlB/ovT89NcivfHXVJ/+ykMkCYmiRbWtI+Pym63Rcn29vF6EVKPzsTvOIdWsVioAewNfzfrxLUBoIWwJyx+18NlMqh0vX6fa70q79LoQKQ/2r1rwuDq5GvzU1JdJp7zAYAQhNFq7k4FA8wJb3+i0jK/Va95P71J9Ft7tIWDBCcqDeXrsUC3Hfov4i4nBNiToBpn/7nfTqQnOVvr/7nvGAkH8AUoxgUyAbw2rH7IuJyXbIiNvuf75oZsdr/XFfNid3+58mA5Kl/ckjLMRvmALinY/qfZS7IAHB3cli7FgAQDux/hhuntgN0ciRAao8GeEU5to3HAkxH93/r5etiuABkuXhsI0AjBbT2OIDDC8D29dAmgCBltOtRAF0hAFuuYN8CcI+lADwOAShgBvgm0B4B8JKC2rg/QCip/+vroJivgBsjsBrgXlb/V0dAKgnA6giI9SlwawTWArxF6oiAmF8DfGvDngCv8vq/cjkoFawBNi0H1wHcYokAz/0AOikyAuNeAPdYJIB87AXwKrP/q/ZHVwEU2v9VY0AqGgGrxoBUNAJWjQGpaASsWg5LTSNgzZ8IVgB8FpyA5x4AXcEAjx0AgkhNRUCqKgErHoiWA9yKTsCHPkBXNMDl7ACiDlB2DVxeBaWqGriiCi4HKDwB6gCvshOweBpYDPAuPAFXbYCmcICnNkDhI2DxQgCApcuA0gEEAF2Ae+kADoCTAyQSQAJIAAkgASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAEkAASQAJIAAkgASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAEkAASQAJIAAkgASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAEkAASQAJIAAkgASSABJAAEkACSAAJIAEkgASQABJAAkgACSABJIAEkAASQAJIAAkgASSABJAAEkACSEAVCfgLW8WrhD7su84AAAAASUVORK5CYII="

/***/ }),

/***/ "./index.css":
/*!*******************!*\
  !*** ./index.css ***!
  \*******************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/css-loader??ref--5-1!./index.css */ "../node_modules/css-loader/index.js??ref--5-1!./index.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "../node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./index.jsx":
/*!*******************!*\
  !*** ./index.jsx ***!
  \*******************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(firebase, ReactDOM, React) {

var _Main = __webpack_require__(/*! components/Main.jsx */ "./components/Main.jsx");

var _Main2 = _interopRequireDefault(_Main);

__webpack_require__(/*! ./index.css */ "./index.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

window.onload = function () {
    var config = {
        apiKey: "AIzaSyAr0JffAcTKwusIh55VafwwXPJuzBDmSW4",
        authDomain: "chat-tw.firebaseapp.com",
        databaseURL: "https://chat-tw.firebaseio.com",
        projectId: "chat-tw",
        storageBucket: "chat-tw.appspot.com",
        messagingSenderId: "531477373158"
    };
    firebase.initializeApp(config);
    ReactDOM.render(React.createElement(_Main2.default, null), document.getElementById('root'));
};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! firebase/app */ "../node_modules/firebase/app/index.js"), __webpack_require__(/*! react-dom */ "../node_modules/react-dom/index.js"), __webpack_require__(/*! react */ "../node_modules/react/index.js")))

/***/ }),

/***/ 0:
/*!******************************************************************************************************************************************************************************************************!*\
  !*** multi @fortawesome/fontawesome @fortawesome/fontawesome-free-solid bootstrap bootstrap/dist/css/bootstrap.min.css firebase/auth firebase/firestore firebase/storage babel-polyfill ./index.jsx ***!
  \******************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! @fortawesome/fontawesome */"../node_modules/@fortawesome/fontawesome/index.es.js");
__webpack_require__(/*! @fortawesome/fontawesome-free-solid */"../node_modules/@fortawesome/fontawesome-free-solid/index.es.js");
__webpack_require__(/*! bootstrap */"../node_modules/bootstrap/dist/js/bootstrap.js");
__webpack_require__(/*! bootstrap/dist/css/bootstrap.min.css */"../node_modules/bootstrap/dist/css/bootstrap.min.css");
__webpack_require__(/*! firebase/auth */"../node_modules/firebase/auth/index.js");
__webpack_require__(/*! firebase/firestore */"../node_modules/firebase/firestore/index.js");
__webpack_require__(/*! firebase/storage */"../node_modules/firebase/storage/index.js");
__webpack_require__(/*! babel-polyfill */"../node_modules/babel-polyfill/lib/index.js");
module.exports = __webpack_require__(/*! ./index.jsx */"./index.jsx");


/***/ })

/******/ });
//# sourceMappingURL=index.bundle.js.map